# README #

### A package version of the equilibrium LAI function ###

* Goal: Predict optimal LAI via g1 under given climatic conditions. 
* Version: 2.0 
* Summary: 
The function uses climate variables to predict long term equilibrium LAI.
The LAI is optimal in term of foliage net carbon export (NCE; GPP - cost).
The model is called optimal g1 because the optimisation is done via g1 (i.e. LAI is a function of g1).
VPD and E are required inputs. But see all the variables in the list.
Result is a vector in following order ("g1", "LAI","Gs","NCG","LUE","GPP").

* Configuration:
There are two important saturation effects in the model: 
1. Beer's Law shows that APAR saturates at high LAI indicating that the marginal return of LAI decreasing; 
2. LUE relationship with g1 (shown in the model hypothesis file) which also has reducing marginal return. 
The balance of those two effects is the major driver of LAI and g1 optimisation. Additional effect on LAI is the leaf construction and respiration cost which decreases with increasing rainfall or increases with PAR (very slightly though).

The new 2.0 version adds transpiration as fraction of evapotranspiration with the fraction being a function of LAI based on Wang et al. 2014 GRL. The new model is then solved by iteration as a result of added complexity. 

### Contribution guidelines ###
The code is constructed with the help from Dr. Remko Duursma, Dr. Martin De Kauwe, and Prof belinda Medlyn. And always the tremendous help from Stackoverflow and other web sources. 

### Who do I talk to? ###

* Contact
Jinyan Yang <jyyoung@live.com> @ WSU

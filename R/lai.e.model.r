#'Equilibrium LAI function
#'
#'The function uses climate variables to predict long term equilibrium LAI.
#'The LAI is optimal in term of foliage net carbon export (NCE; GPP - cost).
#'The model is called optimal g1 because the optimisation is done via g1 (i.e. LAI is a function of g1).
#' @param VPD and E are required. But see all the variables in the list.
#' @return a vector of result in the order of ("g1", "LAI","Gs","NCG","LUE","GPP")
#' @export
#'
#'
g1.lai.e.func <- function (VPD,#kPa
                           E,#mean annuam precip, mm y-1
                           PAR, #Annual PAR; mj par m-2 ground y-1
                           TMAX, #mean Tmax of each month; Celsius
                           theta  = 0.7, #medlyn 2007
                           budyco = TRUE,#when true, use budyco curve and E is a fraction of MAP; Otherwise E=0.8*MAP
                           gammaS25  = 42.75, #ppm; 45 as Mcmurtire or 42.75 as Bernacchi 2001
                           k  = 0.5, #light extinction coeffcient
                           Ca  = 375, #atmospheric co2 concentration;ppm
                           # Jmax25  = 1.8 * 70.0, # mumol s-1 based on leaf N content = 1.8 g N m-2 leaf
                           Jmax25  = 160, #mean of synthetic data set
                           m  = 0, #leaf transmittion coefficient
                           P  = 100, #atmospheric pressure to convert VPD unit, Kpa
                           Rdark  = 1.59, #mumol m-2 s-1; mean from glopresp aust subset
                           SLA = 59.02,#specific leaf are; cm2 leaf g-1 dry mass; globnet wright 2004
                           ll.in = 1.42, # globnet mean in yr
                           R.test=FALSE,#when tru Rdark is a function of MAP otherwise uses ginven value
                           lma.test=FALSE,#when tru LMA is a function of PAR otherwise based on SLA given
                           et.lai.test = TRUE,#when true fraction of T/ET is a function of LAI, otherwise 1
                           r.min = 0.17 #minimum leaf respiration at abient temp; mumol C m-2 s-1; Atkin 2015
){
  mole.weight.c <- 12.0
  g2ummole.c <- 1.0/12.0*10^6
  mole.weight.ch <- 1.6
  mumole2mole <- 10^-6
  days <- 365.25
  h  <- 12.0 * 60.0 * 60.0 # second of day length in a year
  mole2mm.water <- 1.8 * 10^-2
  construction.r.constant <- 1.3
  convertor <- 2.0 * 10^6 # 4.57 mumol PAR MJ-1 or 2 mumol PAR MJ-1 (Sands,1996);
  MOL2MJ <- 4.0 #converts mol of PAR to MJ; 4 MJ mol-1
  Q <- 2.0 * PAR / days #Daily irradiance; Mj total irradiance m-2 ground d-1

  # fraction based on Budyco curve (Zhang et al, 2001)
  # fraction of MAP available to plant
  ifelse(budyco == TRUE,
         fraction <- (E + 2.0*1410.0) / (1.0 + 2.0*1410.0 / E + E/1410.0) / E,
         fraction <- 0.8)

  # rdard as measured as a function of log(map) also from globresp dataset

  ifelse(R.test==TRUE,Rdark <- exp(-0.589 * log(E) + 4.235),Rdark <- Rdark) #rd based on wright 2006

  # set minimum r dark
  # r.min #minimum leaf respiration at abient temp; mol C m-2 s-1; Atkin 2015
  if(is.na(E)==1){
    Rdark <- NA
  }else{
    if(Rdark < r.min) {Rdark <- r.min}
  }

  # gammaS value as func of TMAX (Bernacchi,2001)
  gammaS <- exp(19.02-37.83/(8.314*10^-3*(TMAX + 273.15)))

  Jmax <- Jmax25
  # functions to get LUE ####
  CiFunc <- function (g1) {
    Ca * g1 / (g1 + VPD^0.5)
  }

  #photo rate at saturate light on the top of canopy
  AxFunc <- function(g1){
    Jmax / 4.0 * (CiFunc(g1) - gammaS) / (CiFunc(g1) + 2.0 * gammaS)
  }

  # quntum effect (Ci function cite Belinda's 2000 CJFR paper;medlyn 2007)
  alphaFunc <- function (x){
    0.26/4.0 * (Ca - gammaS) / (Ca + 2.0 * gammaS)
  }

  #q=(π∙k∙α∙Q*gamma)/(2h(1-m)A_x ) Sands 1995 Eq 13
  qFunc <- function(x){
    (pi * k * alphaFunc(x) * Q * convertor) / (2.0 * h * (1.0-m) * AxFunc(x))
  }

  # mast mode lue
  Epsilon <- function(g1){
    qq <- qFunc(g1)

    sin1 <- sin(pi / 24)
    sin2 <- sin(pi * 3 / 24)
    sin3 <- sin(pi * 5 / 24)
    sin4 <- sin(pi * 7 / 24)
    sin5 <- sin(pi * 9 / 24)
    sin6 <- sin(pi * 11 / 24)
    g1 <- sin1 / (1 + qq * sin1 + sqrt((1 + qq * sin1) ^ 2 - 4 * theta * qq * sin1))
    g2 <- sin2 / (1 + qq * sin2 + sqrt((1 + qq * sin2) ^ 2 - 4 * theta * qq * sin2))
    g3 <- sin3 / (1 + qq * sin3 + sqrt((1 + qq * sin3) ^ 2 - 4 * theta * qq * sin3))
    g4 <- sin4 / (1 + qq * sin4 + sqrt((1 + qq * sin4) ^ 2 - 4 * theta * qq * sin4))
    g5 <- sin5 / (1 + qq * sin5 + sqrt((1 + qq * sin5) ^ 2 - 4 * theta * qq * sin5))
    g6 <- sin6 / (1 + qq * sin6 + sqrt((1 + qq * sin6) ^ 2 - 4 * theta * qq * sin6))

    #Trapezoidal rule - seems more accurate
    gg <- 1.0/6.0 * (g1 + g2 + g3 + g4 + g5 + g6)
    eps <- alphaFunc(g1) * gg * pi
    return(eps)
  }

  LUE <- function(g1) {
    Epsilon(g1) * MOL2MJ * mole.weight.c
  }

  # instrinsic tranpiration effciency as in Medlyn 2011
  ITE <- function(g1){
    1 / mole2mm.water * mole.weight.c * mumole2mole / mole.weight.ch * Ca *P / (VPD + g1 * VPD^0.5)
  }

  # # gs  mol H2O m-2 leaf s-1
  # gs <- function(g1){
  #   mole.weight.ch * (1 + g1/VPD^0.5) * GPPfunc(g1) * g2ummole.c  / Ca / LAI(g1) / h / days
  # }

  # f.t.et <- function(lai){1-exp(-b.t*lai)}
  # f.t.et <- function(lai){1-exp(-2*lai)}

  # Wang et al 2014 GRL
  if(et.lai.test == TRUE){
    f.t.et <- function(lai){if(lai > 0){0.77 * lai^0.1}else{0}}
  }else{
    f.t.et <- function(lai)1
  }
  # basiclly mol of water times ite
  GPPfunc <- function(g1,LAI){
    LUE(g1) * PAR * (1-exp(-k*LAI))
  }

  # construction based on SLA following Manzonni et al. 2015;
  # SLA based on Duursma et al. 2015:
  LMA <- 1/SLA*10^4 #g Dry mass m-2 leaf

  # LMA LL based on Wright 2005
  # RAD is W m-2 (daily average) and par is MJ m-2 yr-1 (annual total)
  # to convert PAR to RAD (1w = 1*10^-6 MJ/s):
  # mj2w <- 10^6
  # RAD <- PAR / days / h * mj2w
  ifelse(lma.test==TRUE, LMA <- exp(1.025 * log(PAR) -3.304), LMA <- LMA) #lma based on wright 2005

  # leaf lifespan in year
  ifelse(lma.test==TRUE, LL <- exp(1.143*log(LMA)-5.643), #yr
         LL <- ll.in) #lma based on wright 2005

  LCA <- 0.5 * LMA  #g C m-2 leaf
  #optimasation target
  netfunc <- function(g1,LAI){
    GPPfunc(g1,LAI) -  construction.r.constant * LCA * LAI / LL -
      mole.weight.c * mumole2mole * Rdark * 2 * h * days * LAI
  }

  # solve by iteration
  inter.func <- function(g1,LAI){
    E * fraction * f.t.et(LAI) / mole2mm.water * mole.weight.c * mumole2mole / mole.weight.ch * Ca * P /
      GPPfunc(g1,LAI) / VPD^0.5 - VPD^0.5
  }


  g1.min <- gammaS * VPD^0.5 / (Ca - gammaS) + 0.001

  g1.func <- function(LAI,num.iter = 40){
    g1.vec <- c(2)
    error.vec <- c(100)
    i <- 1
    # #
    #    if(is.na(error[i]) == FALSE){
    #      while (error[i] > 0.001 & i < num.iter){
    #     g1.vec[i+1] <- inter.func(g1.vec[i],LAI)
    #     error[i+1] <- abs(g1.vec[i] - g1.vec[i+1])
    #     i <- i+1
    #    }}else{
    #      g1.vec[i+1] <- NA
    # #    }
    # LAI=10
    if(LAI>0){
      while (error.vec[i] > 0.0001 && i < num.iter){
        g1.vec[i+1] <- inter.func(g1.vec[i],LAI)
        error.vec[i+1] <- abs(g1.vec[i] - g1.vec[i+1])
        i <- i+1
      }
    }else{
      g1.vec[i+1] <- 0
    }

    if (g1.vec[length(g1.vec)] < g1.min){
      g1.vec[length(g1.vec)] <- NA
    }

    return(g1.vec[length(g1.vec)])
  }

  opt.func <- function(LAI){

    g1.it <- g1.func(LAI)

    nce.temp <- netfunc(g1.it,LAI)

    if(is.na(nce.temp) == 0){
      if(nce.temp >= 0 && LAI > 0){
        out.nce <- nce.temp
      }else{
        # -LAI here makes out.nce a declining fucntion of LAI and thus makes optimisation simple
        out.nce <- -LAI
      }
    }else{
      out.nce <- -LAI
    }

    return(out.nce)
  }

  # # gs  mol H2O m-2 leaf s-1
  gs <- function(g1,LAI){
    mole.weight.ch * (1 + g1/VPD^0.5) * GPPfunc(g1,LAI) * g2ummole.c  / Ca / LAI / h / days
  }

  if(is.na(E) == TRUE | is.na(VPD) == TRUE | is.na(PAR) == TRUE | is.na(TMAX) == TRUE){
    lai.opt <- NA
    g1.opt <- NA
    gs.opt <- NA
    GPP.opt <- NA
    NEC.opt <- NA
    t.opt <- NA
    wue.opt <- NA
  }else{
    if(E < 100) {
      max.opt.time <- 600
    }else{
      max.opt.time <- 200
      }
    # lai.opt <- optim(0.005,opt.func,method = "SANN", control = list(fnscale = -1,maxit = max.opt.time))$par
    lai.opt <- optimise(opt.func,interval = c(0.001,12),maximum=TRUE)$maximum
    if(is.na(lai.opt) == FALSE){
      g1.opt <- g1.func(lai.opt)
      gs.opt <- gs(g1.opt,lai.opt)
      GPP.opt <- GPPfunc(g1.opt,lai.opt)
      NEC.opt <- netfunc(g1.opt,lai.opt)
      t.opt <- GPPfunc(g1.opt,lai.opt) / ITE(g1.opt)
      wue.opt <- ITE(g1.opt)
    }else{
      g1.opt <- NA
      gs.opt <- NA
      GPP.opt <- NA
      NEC.opt <- NA
      t.opt <- NA
      wue.opt <- NA
    }

  }

  if(is.na(NEC.opt) == 1 | NEC.opt < 0){
    lai.opt <- NA
    g1.opt <- NA
    gs.opt <- NA
    GPP.opt <- NA
    NEC.opt <- NA
    t.opt <- NA
    wue.opt <- NA
  }
  # result <- data.frame(g1.opt,lai.opt,gs.opt,GPP.opt,NEC.opt,e.opt)

  v <- c(g1=g1.opt, LAI=lai.opt, gs=gs.opt,
         NCE=NEC.opt, t.opt = t.opt, GPP = GPP.opt,WUE = wue.opt)
  return(v)

}
